package com.shustoff.androidchallenge.model.images

import java.io.Serializable

data class Image(
    val url: String,
    val width: Int,
    val height: Int
) : Serializable
package com.shustoff.androidchallenge.model.images

import java.io.Serializable

data class User(
    val name: String,
    val nickname: String,
    val profileUrl: String
) : Serializable
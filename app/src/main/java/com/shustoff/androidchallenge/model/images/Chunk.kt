package com.shustoff.androidchallenge.model.images

data class Chunk<T>(
    val items: List<T>,
    val nextChunkParams: Pagination?
)
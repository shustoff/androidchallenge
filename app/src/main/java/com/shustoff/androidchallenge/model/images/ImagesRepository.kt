package com.shustoff.androidchallenge.model.images

import com.shustoff.androidchallenge.api.GiphyInterface
import com.shustoff.androidchallenge.api.dto.ImageInfoDto
import javax.inject.Inject

class ImagesRepository @Inject constructor(
    private val api: GiphyInterface
) {

    suspend fun getTrending(pagination: Pagination): Chunk<ImageInfo> {
        val response = api.trending(pagination.limit, pagination.offset)

        return Chunk(
            items = response.data.map { fromDto(it) },
            nextChunkParams = pagination
                .takeIf {
                    response.pagination != null &&
                        response.pagination.total > pagination.offset + pagination.limit
                }
                ?.copy(offset = pagination.offset + pagination.limit)
        )
    }

    suspend fun get(id: String): ImageInfo {
        val response = api.byId(id)

        return fromDto(response.data)
    }

    private fun fromDto(
        it: ImageInfoDto
    ): ImageInfo {
        return ImageInfo(
            id = it.id,
            still = Image(
                url = it.images.still.url,
                height = it.images.still.height,
                width = it.images.still.width
            ),
            animated = Image(
                url = it.images.animated.url,
                height = it.images.animated.height,
                width = it.images.animated.width
            ),
            title = it.title,
            user = it.user?.let {
                User(
                    name = it.name,
                    nickname = it.nickname,
                    profileUrl = it.profileUrl
                )
            }
        )
    }
}
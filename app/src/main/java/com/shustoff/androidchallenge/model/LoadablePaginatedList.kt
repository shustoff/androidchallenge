package com.shustoff.androidchallenge.model

import com.shustoff.androidchallenge.api.ApiError
import com.shustoff.androidchallenge.model.images.Chunk
import com.shustoff.androidchallenge.model.images.Pagination

data class LoadablePaginatedList<T>(
    val nextChunkParams: Pagination? = Pagination(),
    val items: List<T> = emptyList(),
    val error: ApiError? = null,
    val loadChunk: suspend (Pagination) -> Chunk<T>
) {

    suspend fun loadWithNextPage(): LoadablePaginatedList<T> {
        return try {
            val newChunk = nextChunkParams?.let { loadChunk.invoke(it) }
            copy(
                nextChunkParams = newChunk?.nextChunkParams,
                error = null,
                items = items + newChunk?.items.orEmpty()
            )
        } catch (e: ApiError) {
            copy(
                error = e
            )
        }
    }
}
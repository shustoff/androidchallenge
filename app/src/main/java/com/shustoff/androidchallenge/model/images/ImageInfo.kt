package com.shustoff.androidchallenge.model.images

import com.shustoff.diffadapter.WithId
import java.io.Serializable

data class ImageInfo(
    override val id: String,
    val still: Image,
    val animated: Image,
    val title: String,
    val user: User?
) : WithId<String>, Serializable
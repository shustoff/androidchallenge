package com.shustoff.androidchallenge.model.images

data class Pagination(
    val limit: Int = 30,
    val offset: Int = 0
)
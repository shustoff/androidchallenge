package com.shustoff.androidchallenge.extensions

import androidx.recyclerview.widget.RecyclerView

class RecyclerViewScrollWrapper(
    private val callback: OnScrollListener
) : RecyclerView.OnScrollListener() {

    private var lastVisibleCount: Int = 0
    private var lastFirstVisibleIndex: Int = 0
    private var lastTotalCount: Int = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)
        val manager = recyclerView.layoutManager ?: return

        val totalCount = manager.itemCount
        val visibleCount = manager.childCount

        val firstChild = manager.getChildAt(0) ?: return
        val firstVisibleIndex = manager.getPosition(firstChild)

        if (lastVisibleCount != visibleCount ||
            lastFirstVisibleIndex != firstVisibleIndex ||
            lastTotalCount != totalCount
        ) {
            lastVisibleCount = visibleCount
            lastFirstVisibleIndex = firstVisibleIndex
            lastTotalCount = totalCount

            callback.onScroll(
                firstVisibleIndex = firstVisibleIndex,
                visibleItemsCount = visibleCount,
                totalCount = totalCount
            )
        }
    }

    interface OnScrollListener {

        fun onScroll(
            totalCount: Int,
            firstVisibleIndex: Int,
            visibleItemsCount: Int
        )
    }
}
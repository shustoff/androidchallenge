package com.shustoff.androidchallenge.extensions

import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.shustoff.mvp.navigation.transitions.EnterTransitionHelper

fun <TranscodeType> RequestBuilder<TranscodeType>.startPostponedTransitionWhenLoaded(
    transitionHelper: EnterTransitionHelper
): RequestBuilder<TranscodeType> {
    return listener(object : RequestListener<TranscodeType> {
        override fun onLoadFailed(
            e: GlideException?,
            model: Any?,
            target: Target<TranscodeType>?,
            isFirstResource: Boolean
        ): Boolean {
            transitionHelper.startPostponed()
            return false
        }

        override fun onResourceReady(
            resource: TranscodeType?,
            model: Any?,
            target: Target<TranscodeType>?,
            dataSource: DataSource?,
            isFirstResource: Boolean
        ): Boolean {
            transitionHelper.startPostponed()
            return false
        }
    })
}
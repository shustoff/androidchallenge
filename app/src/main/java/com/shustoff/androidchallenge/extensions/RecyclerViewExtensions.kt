package com.shustoff.androidchallenge.extensions

import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator

fun RecyclerView.disableDefaultChangeAnimation() {
    itemAnimator?.let {
        if (it is SimpleItemAnimator) {
            it.supportsChangeAnimations = false
        }
    }
}

fun RecyclerView.addOnScrollNearEndListener(
    spanCount: Int,
    linesCountBeforeEnd: Int = 2,
    onEndReached: () -> Unit
) {
    addOnScrollListener(RecyclerViewScrollWrapper(
        object : RecyclerViewScrollWrapper.OnScrollListener {
            override fun onScroll(
                totalCount: Int,
                firstVisibleIndex: Int,
                visibleItemsCount: Int
            ) {
                val linesToEnd = (totalCount - firstVisibleIndex - visibleItemsCount) / spanCount
                if (linesToEnd <= linesCountBeforeEnd) {
                    onEndReached()
                }
            }
        }
    ))
}
package com.shustoff.androidchallenge.navigation

import android.app.Activity
import android.app.Application
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import com.shustoff.androidchallenge.R
import com.shustoff.androidchallenge.screens.imageInfo.ImageInfoActivity
import com.shustoff.mvp.navigation.RouterBase
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Router @Inject constructor(
    application: Application
) : RouterBase<Screen>(application) {

    private var currentActivity: Activity? = null

    init {
        application.registerActivityLifecycleCallbacks(LifecycleCallbacks())
    }

    override fun ScreenLaunchingContext<Screen>.goToScreen(screen: Screen) {
        when (screen) {
            is Screen.Image -> startActivity(ImageInfoActivity::class, screen)

            is Screen.Profile -> {
                currentActivity?.takeUnless { it.isFinishing }?.also { from ->
                    val customTabsIntent = CustomTabsIntent.Builder()
                        .addDefaultShareMenuItem()
                        .setToolbarColor(
                            ContextCompat.getColor(from, R.color.colorPrimary)
                        )
                        .setShowTitle(true)
                        .build()

                    val uri = Uri.parse(screen.url)
                    customTabsIntent.intent.data = uri

                    if (hasActivityForIntent(from, customTabsIntent.intent)) {
                        customTabsIntent.launchUrl(from, uri)
                    } else {
                        val browserIntent = Intent(Intent.ACTION_VIEW, uri)

                        if (hasActivityForIntent(from, browserIntent)) {
                            from.startActivity(browserIntent)
                        } else {
                            Toast.makeText(from, R.string.browser_not_supported, Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
            }
        }
    }

    private fun hasActivityForIntent(
        activity: Activity,
        intent: Intent
    ): Boolean {
        val infoList = activity.packageManager
            .queryIntentActivities(intent, 0)

        return infoList.isNotEmpty()
    }

    fun navigateUp() {
        if (currentActivity?.onNavigateUp() == false) {
            currentActivity?.finish()
        }
    }

    private inner class LifecycleCallbacks : Application.ActivityLifecycleCallbacks {
        override fun onActivityPaused(activity: Activity?) {
            if (currentActivity == activity) {
                currentActivity = null
            }
        }

        override fun onActivityResumed(activity: Activity?) {
            currentActivity = activity
        }

        override fun onActivityStarted(activity: Activity?) {
        }

        override fun onActivityDestroyed(activity: Activity?) {
        }

        override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
        }

        override fun onActivityStopped(activity: Activity?) {
        }

        override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
        }
    }
}
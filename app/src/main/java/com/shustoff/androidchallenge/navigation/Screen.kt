package com.shustoff.androidchallenge.navigation

import com.shustoff.androidchallenge.model.images.ImageInfo
import java.io.Serializable

sealed class Screen : Serializable {

    sealed class Image : Screen() {

        data class Loaded(val image: ImageInfo) : Image()

        data class ById(val id: String): Image()
    }

    data class Profile(val url: String) : Screen()
}
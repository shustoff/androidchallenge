package com.shustoff.androidchallenge

import android.app.Application
import android.content.Context
import android.content.res.Resources
import com.shustoff.androidchallenge.api.GiphyInterface
import com.shustoff.androidchallenge.api.GiphyInterfaceProvider
import com.shustoff.androidchallenge.api.RetrofitProvider
import com.shustoff.mvp.di.Di
import com.shustoff.mvp.di.module
import retrofit2.Retrofit

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        Di.init(
            this,
            module {
                instance<Application>(this@App)
                instance<Context>(this@App)
                instance<Resources>(resources)
                provided<Retrofit>(singleton = true).by<RetrofitProvider>()
                provided<GiphyInterface>(singleton = true).by<GiphyInterfaceProvider>()
            }
        )
    }
}
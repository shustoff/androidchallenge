package com.shustoff.androidchallenge.api.dto

import com.google.gson.annotations.SerializedName

class PaginationDto(
    @SerializedName("total_count") val total: Int,
    val offset: Int,
    val count: Int
)
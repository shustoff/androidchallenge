package com.shustoff.androidchallenge.api.interceptors

import android.content.res.Resources
import com.shustoff.androidchallenge.R
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

class ClientAuthInterceptor @Inject constructor(
    resources: Resources
) : Interceptor {

    private val keyValue = resources.getString(R.string.api_key)

    override fun intercept(chain: Interceptor.Chain): Response {
        return chain.proceed(
            chain.request().newBuilder()
                .url(
                    chain.request().url().newBuilder()
                        .addQueryParameter(apiKey, keyValue)
                        .build()
                )
                .build()
        )
    }

    companion object {
        private const val apiKey = "api_key"
    }
}
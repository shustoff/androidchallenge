package com.shustoff.androidchallenge.api

import retrofit2.Retrofit
import retrofit2.create
import javax.inject.Inject
import javax.inject.Provider

class GiphyInterfaceProvider @Inject constructor(
    private val retrofit: Retrofit
) : Provider<GiphyInterface> {

    override fun get(): GiphyInterface = retrofit.create()
}
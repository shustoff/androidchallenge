package com.shustoff.androidchallenge.api

import com.shustoff.androidchallenge.api.dto.ImageInfoDto
import com.shustoff.androidchallenge.api.dto.ResponseDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GiphyInterface {

    @GET("/v1/gifs/trending")
    suspend fun trending(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ) : ResponseDto<List<ImageInfoDto>>

    @GET("/v1/gifs/{id}")
    suspend fun byId(
        @Path("id") id: String
    ) : ResponseDto<ImageInfoDto>
}
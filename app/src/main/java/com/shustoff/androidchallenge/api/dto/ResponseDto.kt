package com.shustoff.androidchallenge.api.dto

class ResponseDto<T>(
    val data: T,
    val pagination: PaginationDto?
)
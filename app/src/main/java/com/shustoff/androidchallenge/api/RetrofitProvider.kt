package com.shustoff.androidchallenge.api

import android.content.res.Resources
import com.shustoff.androidchallenge.R
import com.shustoff.androidchallenge.api.interceptors.ClientAuthInterceptor
import com.shustoff.androidchallenge.api.interceptors.ThrowApiErrorInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Provider

class RetrofitProvider @Inject constructor(
    private val resources: Resources,
    private val authInterceptor: ClientAuthInterceptor,
    private val errorInterceptor: ThrowApiErrorInterceptor
) : Provider<Retrofit> {

    override fun get(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(resources.getString(R.string.server_url))
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(authInterceptor)
                    .addInterceptor(errorInterceptor)
                    .build()
            )
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}
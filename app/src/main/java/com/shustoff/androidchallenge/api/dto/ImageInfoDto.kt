package com.shustoff.androidchallenge.api.dto

import com.google.gson.annotations.SerializedName

class ImageInfoDto(
    val id: String,
    val images: ImagesDto,
    val title: String,
    val user: UserDto?
) {

    class ImagesDto(
        @SerializedName("480w_still") val still: ImageDto,
        @SerializedName("downsized_medium") val animated: ImageDto
    )

    class ImageDto(
        val url: String,
        val width: Int,
        val height: Int
    )

    class UserDto(
        @SerializedName("display_name") val name: String,
        @SerializedName("username") val nickname: String,
        @SerializedName("profile_url") val profileUrl: String
    )
}
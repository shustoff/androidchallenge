package com.shustoff.androidchallenge.api

import android.content.res.Resources
import com.google.gson.GsonBuilder
import com.shustoff.androidchallenge.R
import com.shustoff.androidchallenge.api.dto.ErrorResponseDto
import java.io.IOException

sealed class ApiError(cause: Throwable? = null) : IOException(cause) {

    abstract fun getUserFriendlyMessage(resources: Resources): String

    class Detailed(
        val status: Int,
        val apiMessage: String?,
        val url: String?
    ) : ApiError() {

        override fun getUserFriendlyMessage(resources: Resources): String =
            apiMessage ?: resources.getString(R.string.error_code, status)
    }

    class NoConnection(
        val url: String?,
        cause: Throwable
    ) : ApiError(cause) {

        override fun getUserFriendlyMessage(resources: Resources): String =
            resources.getString(R.string.no_internet)
    }

    companion object {

        private val gson = GsonBuilder().create()

        fun parse(response: okhttp3.Response): ApiError? {
            val errorResponse = try {
                response.body()?.charStream()
                    ?.let {
                        gson.fromJson(it, ErrorResponseDto::class.java)
                    }
            } catch (e: Throwable) {
                null
            }

            return Detailed(
                apiMessage = errorResponse?.meta?.msg,
                status = response.code(),
                url = response.request().url().toString()
            )
        }
    }
}
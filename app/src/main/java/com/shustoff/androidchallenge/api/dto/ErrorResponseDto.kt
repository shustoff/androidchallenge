package com.shustoff.androidchallenge.api.dto

class ErrorResponseDto(
    val meta: MetaDto?
) {

    class MetaDto(
        val msg: String?
    )
}
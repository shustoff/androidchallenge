package com.shustoff.androidchallenge.api.interceptors

import com.shustoff.androidchallenge.api.ApiError
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import javax.inject.Inject

class ThrowApiErrorInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val response = try {
            chain.proceed(chain.request())
        } catch (e: IOException) {
            throw ApiError.NoConnection(
                chain.request().url().toString(),
                e
            )
        }

        if (response.code() >= 400) {
            ApiError.parse(response)?.let { throw it }
        }

        return response
    }
}
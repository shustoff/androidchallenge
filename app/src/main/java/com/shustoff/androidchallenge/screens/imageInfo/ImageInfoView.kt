package com.shustoff.androidchallenge.screens.imageInfo

import android.content.res.Resources
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.shustoff.androidchallenge.R
import com.shustoff.androidchallenge.extensions.startPostponedTransitionWhenLoaded
import com.shustoff.mvp.MvpView
import com.shustoff.mvp.ViewStateEmitter
import com.shustoff.mvp.inflater.RootViewInflater
import com.shustoff.mvp.navigation.transitions.EnterTransitionHelper
import kotlinx.android.synthetic.main.item_image.view.image
import kotlinx.android.synthetic.main.screen_image.view.*
import javax.inject.Inject

class ImageInfoView @Inject constructor(
    stateEmitter: ViewStateEmitter<ImageInfoState>,
    inflater: RootViewInflater,
    private val resources: Resources,
    private val transitionHelper: EnterTransitionHelper
) : MvpView<ImageInfoState>(stateEmitter) {

    private val view = inflater.inflateRootView(R.layout.screen_image)

    init {
        view.user.setOnClickListener {
            state?.onUserClick?.invoke()
        }
        view.toolbar.setNavigationOnClickListener {
            state?.onNavigateUp?.invoke()
        }
        transitionHelper.postpone()
    }

    override fun onRenderState(state: ImageInfoState) {
        view.toolbar.title = state.image?.title ?: resources.getString(R.string.image_info)

        view.image.isVisible = null != state.image
            ?.also {
                Glide.with(view.image)
                    .load(state.image.animated.url)
                    .thumbnail(
                        Glide.with(view.image)
                            .load(state.image.still.url)
                            .dontTransform()
                            .onlyRetrieveFromCache(true)
                    )
                    .startPostponedTransitionWhenLoaded(transitionHelper)
                    .into(view.image)
            }

        view.user.isVisible = null != state.image?.user
            ?.also {
                view.user.text = resources.getString(R.string.by_user, it.name, it.nickname)
            }

        view.error.isVisible = null != state.error
            ?.also {
                view.error.text = it.getUserFriendlyMessage(resources)
            }
    }
}
package com.shustoff.androidchallenge.screens.trending

import android.view.View
import androidx.constraintlayout.widget.ConstraintSet
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.shustoff.androidchallenge.R
import com.shustoff.androidchallenge.model.images.ImageInfo
import com.shustoff.diffadapter.TypedViewHolder
import com.shustoff.mvp.navigation.TransitionInfo
import kotlinx.android.synthetic.main.item_image.view.*

class ImageViewHolder(
    itemView: View,
    onItemClick: ((ImageInfo) -> Unit)?
) : TypedViewHolder<ImageInfo>(itemView, onItemClick) {

    private val constraintSet = ConstraintSet()
    private val requestOptions = RequestOptions()
        .placeholder(R.drawable.ic_placeholder)

    override fun bind(item: ImageInfo) {
        constraintSet.run {
            clone(itemView.constraint)
            setDimensionRatio(R.id.image, "H,${item.still.width}:${item.still.height}")
            applyTo(itemView.constraint)
        }

        Glide.with(itemView)
            .setDefaultRequestOptions(requestOptions)
            .load(item.still.url)
            .into(itemView.image)
    }

    fun getTransition() = TransitionInfo.from(itemView.image to "image")
}
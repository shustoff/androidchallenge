package com.shustoff.androidchallenge.screens.trending

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shustoff.mvp.di.viewModule
import com.shustoff.mvp.mvp

class TrendingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mvp<TrendingPresenter, TrendingView>()
            .onViewCreated(viewModule())
    }
}

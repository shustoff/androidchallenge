package com.shustoff.androidchallenge.screens.imageInfo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.shustoff.androidchallenge.navigation.Screen
import com.shustoff.mvp.di.module
import com.shustoff.mvp.di.viewModule
import com.shustoff.mvp.mvp
import com.shustoff.mvp.navigation.ScreenActivity

class ImageInfoActivity : AppCompatActivity(), ScreenActivity<Screen.Image> {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mvp<ImageInfoPresenter, ImageInfoView>(
            module {
                instance<Screen.Image>(screen)
            }
        ).onViewCreated(viewModule())
    }

    override fun buildScreen(): Screen.Image {
        return Screen.Image.ById(
            id = intent?.data?.pathSegments?.lastOrNull()
                ?: throw Exception("Invalid deeplink ${intent?.data}")
        )
    }
}
package com.shustoff.androidchallenge.screens.trending

import com.shustoff.androidchallenge.model.LoadablePaginatedList
import com.shustoff.androidchallenge.model.images.ImageInfo
import com.shustoff.androidchallenge.model.images.ImagesRepository
import com.shustoff.androidchallenge.navigation.Router
import com.shustoff.androidchallenge.navigation.Screen
import com.shustoff.mvp.MvpPresenter
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import javax.inject.Inject

class TrendingPresenter @Inject constructor(
    repository: ImagesRepository,
    private val router: Router
) : MvpPresenter<TrendingState>() {

    private val emptyList = LoadablePaginatedList(loadChunk = repository::getTrending)
    private var list = emptyList
    private var currentJob: Job? = null

    init {
        refresh()
    }

    private fun onImageClick(image: ImageInfo) {
        router.goTo(Screen.Image.Loaded(image))
    }

    private fun refresh() {
        currentJob?.cancel()
        loadListItems(emptyList, TrendingState.LoadingType.INITIAL)
    }

    private fun onScrollNearEnd() {
        if (currentJob == null && list.nextChunkParams != null) {
            loadListItems(list, TrendingState.LoadingType.PAGINATION)
        }
    }

    private fun loadListItems(
        listToUpdate: LoadablePaginatedList<ImageInfo>,
        loadingType: TrendingState.LoadingType
    ) {
        currentJob = launch {
            updateState(loading = loadingType)
            list = listToUpdate.loadWithNextPage()
            updateState()
        }
        currentJob?.invokeOnCompletion {
            currentJob = null
        }
    }

    private fun updateState(loading: TrendingState.LoadingType? = null) {
        renderState(
            TrendingState(
                onScrollNearEnd = ::onScrollNearEnd,
                onImageClick = ::onImageClick,
                onPullToRefresh = ::refresh,
                loading = loading,
                items = list.items,
                error = list.error
            )
        )
    }
}
package com.shustoff.androidchallenge.screens.trending

import android.content.res.Resources
import androidx.core.view.isVisible
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.shustoff.androidchallenge.R
import com.shustoff.androidchallenge.api.ApiError
import com.shustoff.androidchallenge.extensions.addOnScrollNearEndListener
import com.shustoff.androidchallenge.extensions.disableDefaultChangeAnimation
import com.shustoff.androidchallenge.model.images.ImageInfo
import com.shustoff.androidchallenge.navigation.Screen
import com.shustoff.diffadapter.StubViewHolder
import com.shustoff.diffadapter.diffAdapter
import com.shustoff.mvp.MvpView
import com.shustoff.mvp.ViewStateEmitter
import com.shustoff.mvp.inflater.RootViewInflater
import com.shustoff.mvp.navigation.RouteAnimators
import com.shustoff.mvp.render.renderer
import kotlinx.android.synthetic.main.screen_trending.view.*
import javax.inject.Inject

class TrendingView @Inject constructor(
    stateEmitter: ViewStateEmitter<TrendingState>,
    inflater: RootViewInflater,
    private val resources: Resources,
    animators: RouteAnimators
) : MvpView<TrendingState>(stateEmitter) {

    private val view = inflater.inflateRootView(R.layout.screen_trending)

    private var lastClickedImage: ImageInfo? = null

    private val adapter = diffAdapter {
        register<ImageInfo>(R.layout.item_image) {
            ImageViewHolder(it, ::onClick)
        }
        register<LoadingIndicator>(R.layout.item_loading, fullWidth = true) {
            StubViewHolder(it)
        }
    }

    private var lastShownSnackbar: Snackbar? = null
    private val errorRenderer = renderer<ApiError?> {
        val message = it?.getUserFriendlyMessage(resources)
        if (message != null) {
            lastShownSnackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
                .also {
                    it.show()
                }
        } else {
            lastShownSnackbar?.dismiss()
            lastShownSnackbar = null
        }
    }

    init {
        val spanCount = resources.getInteger(R.integer.trending_span_count)
        view.list.layoutManager = StaggeredGridLayoutManager(
            spanCount,
            StaggeredGridLayoutManager.VERTICAL
        )
        view.list.disableDefaultChangeAnimation()
        view.list.addOnScrollNearEndListener(spanCount) {
            state?.onScrollNearEnd?.invoke()
        }
        view.swipeToRefresh.setOnRefreshListener {
            state?.onPullToRefresh?.invoke()
        }

        animators.register<Screen.Image.Loaded> { screen ->
            adapter.items.indexOf(screen.image)
                .takeIf { it >= 0 }
                ?.let {
                    view.list.findViewHolderForAdapterPosition(it) as? ImageViewHolder
                }
                ?.getTransition()
        }
    }

    override fun onRenderState(state: TrendingState) {
        if (view.swipeToRefresh.isRefreshing) {
            view.swipeToRefresh.isRefreshing = state.loading == TrendingState.LoadingType.INITIAL
        }

        view.loadingIndicator.isVisible = !view.swipeToRefresh.isRefreshing &&
            state.loading == TrendingState.LoadingType.INITIAL &&
            state.items.isEmpty()

        errorRenderer.render(state.error)

        if (!view.swipeToRefresh.isRefreshing &&
            state.items.isNotEmpty() &&
            state.loading == TrendingState.LoadingType.PAGINATION
        ) {
            adapter.show(state.items + LoadingIndicator)
        } else {
            adapter.show(state.items)
        }

        if (view.list.adapter != adapter) {
            view.list.adapter = adapter
        }
    }

    private fun onClick(image: ImageInfo) {
        lastClickedImage = image
        state?.onImageClick?.invoke(image)
    }

    object LoadingIndicator
}
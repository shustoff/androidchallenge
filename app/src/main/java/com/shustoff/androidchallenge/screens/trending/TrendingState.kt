package com.shustoff.androidchallenge.screens.trending

import com.shustoff.androidchallenge.api.ApiError
import com.shustoff.androidchallenge.model.images.ImageInfo

data class TrendingState(
    val items: List<ImageInfo>,
    val error: ApiError?,
    val onPullToRefresh: () -> Unit,
    val onScrollNearEnd: () -> Unit,
    val onImageClick: (ImageInfo) -> Unit,
    val loading: LoadingType?
) {

    enum class LoadingType {
        INITIAL,
        PAGINATION
    }
}
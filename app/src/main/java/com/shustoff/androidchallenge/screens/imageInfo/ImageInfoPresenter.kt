package com.shustoff.androidchallenge.screens.imageInfo

import com.shustoff.androidchallenge.api.ApiError
import com.shustoff.androidchallenge.model.images.ImageInfo
import com.shustoff.androidchallenge.model.images.ImagesRepository
import com.shustoff.androidchallenge.navigation.Router
import com.shustoff.androidchallenge.navigation.Screen
import com.shustoff.mvp.MvpPresenter
import kotlinx.coroutines.launch
import javax.inject.Inject

class ImageInfoPresenter @Inject constructor(
    screen: Screen.Image,
    private val router: Router,
    private val repository: ImagesRepository
) : MvpPresenter<ImageInfoState>() {

    private var image: ImageInfo? = null

    init {
        when (screen) {
            is Screen.Image.Loaded -> {
                renderImage(screen.image)
            }

            is Screen.Image.ById -> {
                launch {
                    try {
                        renderImage(repository.get(screen.id))
                    } catch (e: ApiError) {
                        renderState(error = e)
                    }
                }
            }
        }
    }

    private fun renderImage(image: ImageInfo) {
        this.image = image
        renderState(image)
    }

    private fun renderState(image: ImageInfo? = null, error: ApiError? = null) {
        renderState(
            ImageInfoState(
                image = image,
                error = error,
                onUserClick = ::onUserClick,
                onNavigateUp = ::onNavigateUpClick
            )
        )
    }

    private fun onNavigateUpClick() {
        router.navigateUp()
    }

    private fun onUserClick() {
        image?.user?.profileUrl?.also { router.goTo(Screen.Profile(it)) }
    }
}
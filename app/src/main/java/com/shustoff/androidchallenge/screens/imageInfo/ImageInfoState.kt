package com.shustoff.androidchallenge.screens.imageInfo

import com.shustoff.androidchallenge.api.ApiError
import com.shustoff.androidchallenge.model.images.ImageInfo

class ImageInfoState(
    val image: ImageInfo?,
    val error: ApiError? = null,
    val onUserClick: () -> Unit,
    val onNavigateUp: () -> Unit
)